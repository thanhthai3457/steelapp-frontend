import React, { forwardRef, useImperativeHandle } from 'react'
import { NotificationContainer, NotificationManager } from 'react-notifications'

const Notification = forwardRef((props, ref) => {
  useImperativeHandle(ref, () => ({
    show: (type, message, title, time) => {
      NotificationManager[`${type}`](
        message,
        title,
        time
      )
    }
  }))

  return (
    <NotificationContainer />
  )
})

export default Notification
