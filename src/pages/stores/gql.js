import gql from 'graphql-tag'

const GET_STORES = gql`
  {
    getStores {
      _id
      code
      name
      mobile
      address
      isShop
    }
  }
`

const CREATE_STORE = gql`
  mutation createStoreShop ($info: StoreShopInfo!) {
    createStoreShop (info: $info)
  }
`

const UPDATE_STORE = gql`
  mutation updateStoreShop ($id: ID!, $info: StoreShopInfo!) {
    updateStoreShop (id: $id, info: $info)
  }
`

const REMOVE_STORE = gql`
  mutation removeStoreShop ($ids: [ID!]!) {
    removeStoreShop (ids: $ids)
  }
`

export { GET_STORES, CREATE_STORE, REMOVE_STORE, UPDATE_STORE }