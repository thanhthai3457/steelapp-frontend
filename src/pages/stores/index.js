import React, { useState, useEffect, useMemo, useRef, useCallback } from 'react'
import Grid from 'components/grid/index.js'
import { graphql } from '@apollo/react-hoc'
import { useMutation } from '@apollo/react-hooks'
import { PlusOutlined } from '@ant-design/icons'
import { GET_STORES, REMOVE_STORE } from './gql'
import ModalForm from './modal'
import { Modal } from 'antd'

const StoreShop = props => {
  const [dataStore, setDataStore] = useState([])
  const gridApi = useRef(null)
  const modalRef = useRef(null)

  const [removeStore] = useMutation(REMOVE_STORE)

  const { notification } = props

  useEffect(() => {
    if (props.data.loading) { props.preloader.show() } else {
      setTimeout(() => {
        props.preloader.hide()
      }, 2000)
    }
    if (props.data.getStores) {
      setDataStore(props.data.getStores)
    }
    return () => {
      props.preloader.hide()
    }
  }, [props.data, props.preloader])

  const refetch = () => {
    props.data.refetch()
  }

  const handleAddClick = useCallback(() => {
    modalRef.current.open()
  }, [])

  const handleEditClick = useCallback((slt) => {
    modalRef.current.open(slt)
  }, [])

  const handleRemoveClick = useCallback((slt) => {
    Modal.confirm({
      title: 'Xác nhận xóa kho!',
      content: `Bạn chắc chắn muốn xóa ${slt.length > 1 ? 'những ' : ''} kho này?`,
      cancelText: 'Đóng',
      centered: true,
      onOk: () => {
        removeStore({
          variables: {
            ids: slt.map(e => e._id)
          }
        })
          .then(res => {
            if (!res.errors || res.data.removeStoreShop === 'success') {
              notification.show(
                'success',
                `Xóa kho thành công!`,
                'Thành công!',
                3000
              )
              setTimeout(() => {
                refetch()
              }, 1000)
            } else {
              const mes = res.errors.message || ''
              if (mes.includes('not exist')) {
                notification.show(
                  'error',
                  `Kho không tồn tại!`,
                  'Thất bại!',
                  3000
                )
                setTimeout(() => {
                  refetch()
                }, 1000)
                return
              }
              notification.show(
                'error',
                `Xóa kho thất bại!`,
                'Thất bại!',
                3000
              )
            }
          })
          .catch(err => {
            console.log(err)
            notification.show(
              'error',
              `Xóa kho thất bại!`,
              'Thất bại!',
              3000
            )
          })
      },
      okText: 'Xác nhận'
    })
  }, [removeStore])

  const gridOptions = useMemo(() => ({
    paginationNumberFormatter (params) {
      return `[${params.value.toLocaleString()}]`
    },
    columnDefs: [
      {
        headerName: 'Mã kho',
        field: 'code'
      },
      {
        headerName: 'Tên kho',
        field: 'name'
      },
      {
        headerName: 'Số điện thoại',
        field: 'mobile'
      },
      {
        headerName: 'Địa chỉ',
        field: 'address'
      }
    ],
    actionDefs: [
      {
        action: 'add',
        type: 'default',
        icon: <PlusOutlined />,
        tooltip: 'Thêm kho mới',
        onClick: handleAddClick
      },
      {
        action: 'update',
        type: 'single',
        tooltip: 'Chỉnh sửa',
        onClick: (selectecRows) => handleEditClick(selectecRows[0])
      },
      {
        action: 'delete',
        type: 'multiple',
        onClick: (selectecRows) => handleRemoveClick(selectecRows)
      }
    ],
    defaultColDef: {
      resizable: true,
      sortable: true,
      filter: true
    },
    rowSelection: 'multiple',
    rowMultiSelectWithClick: true,
    floatingFilter: true,
    pagination: true,
    defaultPageSize: 50
  }), [handleAddClick, handleEditClick, handleRemoveClick])

  return (
    <div>
      <div className='steelapp-toolbar'>
        <div className='steelapp-title-component'>Kho / Cửa hàng</div>
      </div>
      <div className='steelapp-content'>
        <Grid
          {...gridOptions}
          data={dataStore}
          gridApi={gridApi}
        />
      </div>
      <ModalForm
        refetch={refetch}
        ref={modalRef}
        noti={props.notification}
      />
    </div>
  )
}

export default graphql(
  GET_STORES,
  {
    options: {
      fetchPolicy: 'network-only'
    }
  }
)(StoreShop)