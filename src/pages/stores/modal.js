import React, {
  useReducer, useImperativeHandle,
  forwardRef, useCallback, useMemo
} from 'react'
import { useMutation } from '@apollo/react-hooks'
import { Modal, Form, Input, Checkbox } from 'antd'
import { CREATE_STORE, UPDATE_STORE } from './gql'

const reducer = (state, action) => {
  if (action.type) {
    switch (action.type) {
      default:
        return state
    }
  }
  return {
    ...state,
    ...action
  }
}

const ModalForm = React.memo(forwardRef((props, ref) => {
  const [form] = Form.useForm()
  const { noti } = props

  const layout = useMemo(() => ({
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  }), [])
  // const tailLayout = {
  //   wrapperCol: { offset: 8, span: 16 },
  // }

  const [state, setState] = useReducer(reducer, {
    visible: false,
    selectedRow: {},
    checkShop: false
  })

  const [createStore] = useMutation(CREATE_STORE)
  const [updateStore] = useMutation(UPDATE_STORE)

  const { visible, selectedRow, checkShop } = state

  const openModal = useCallback((val) => {
    const { setFieldsValue } = form
    setFieldsValue({
      ...(typeof val === 'object' && val) 
    })
    setState({
      visible: true,
      selectedRow: (val && val._id) ? val : {},
      checkShop: (val && val._id) ? val.isShop : false
    })
  }, [form])

  const closeModal = useCallback(() => {
    const { resetFields } = form
    resetFields()
    setState({
      visible: false,
      selectedRow: {},
      checkShop: false
    })
  }, [form])

  const handleOk = useCallback(() => {
    const { validateFields } = form
    validateFields().then(values => {
      const info = {
        ...values,
        isShop: checkShop
      }
      if (selectedRow && selectedRow._id) {
        updateStore({
          variables: {
            id: selectedRow._id,
            info
          }
        })
          .then(res => {
            if (!res.errors) {
              if (!res.data.updateStoreShop || !(res.data.updateStoreShop === 'success')) {
                noti.show(
                  'error',
                  `Cập nhật không thành công!`,
                  'Thất bại!',
                  3000
                )
                return
              }
              noti.show(
                'success',
                `Cập nhật thành công!`,
                'Hoàn thành!',
                3000
              )
              setTimeout(() => {
                closeModal()
                props.refetch()
              }, 1000)
            } else {
              if (res.errors.message.includes('already exist')) {
                noti.show(
                  'error',
                  `Kho có mã ${values.code} đã tồn tại!`,
                  'Thất bại!',
                  3000
                )
                return
              }
              noti.show(
                'error',
                `Cập nhật thất bại!`,
                'Thất bại!',
                3000
              )
            }
          })
          .catch(er => console.log(er))
      } else {
        createStore({
          variables: {
            info
          }
        })
          .then(res => {
            if (!res.errors) {
              if (!res.data.createStoreShop || !(res.data.createStoreShop === 'success')) {
                noti.show(
                  'error',
                  `Thêm kho không thành công!`,
                  'Thất bại!',
                  3000
                )
                return
              }
              noti.show(
                'success',
                `Thêm kho ${values.name} thành công!`,
                'Hoàn thành!',
                3000
              )
              setTimeout(() => {
                closeModal()
                props.refetch()
              }, 1000)
            } else {
              if (res.errors.message.includes('already exist')) {
                noti.show(
                  'error',
                  `Kho có mã ${values.code} đã tồn tại!`,
                  'Thất bại!',
                  3000
                )
                return
              }
              noti.show(
                'error',
                `Thêm kho thất bại!`,
                'Thất bại!',
                3000
              )
            }
          })
          .catch(er => console.log(er))
      }
    })
  }, [form, checkShop, createStore, noti, closeModal, props, selectedRow])

  useImperativeHandle(ref, () => (
    {
      open: openModal,
      close: closeModal
    }
  ))

  const onChangeShop = useCallback(() => {
    setState({
      checkShop: !checkShop
    }) 
  }, [checkShop])

  return (
    <Modal
      visible={visible}
      title={selectedRow._id ? selectedRow.name : 'Thêm kho mới'}
      centered
      onCancel={closeModal}
      onOk={handleOk}
    >
      <Form
        {...layout}
        className='storeForm'
        form={form}
        name='basic'
        initialValues={{
          remember: true
        }}
      >
        <Form.Item
          label='Mã kho'
          name='code'
          rules={[
            {
              required: true,
              message: 'Vui lòng không để trống mã kho / cửa hàng'
            },
            {
              pattern: /^[^\s]/,
              message: 'Vui lòng không sử dụng khoảng trắng đầu dòng'
            },
            {
              pattern: /^[a-zA-Z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]+$/,
              message: 'Vui lòng không sử dụng kí tự đặc biệt'
            }
          ]}
        >
          <Input placeholder='Nhập mã kho' />
        </Form.Item>
        <Form.Item
          label='Tên kho'
          name='name'
          rules={[
            {
              required: true,
              message: 'Vui lòng không để trống mã kho / cửa hàng'
            },
            {
              pattern: /^[^\s]/,
              message: 'Vui lòng không sử dụng khoảng trắng đầu dòng'
            },
            {
              pattern: /^[a-zA-Z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]+$/,
              message: 'Vui lòng không sử dụng kí tự đặc biệt'
            }
          ]}
        >
          <Input placeholder='Nhập tên kho' />
        </Form.Item>
        <Form.Item
          label='Số điện thoại'
          name='mobile'
        >
          <Input placeholder='Nhập số điện thoại' />
        </Form.Item>
        <Form.Item
          label='Địa chỉ'
          name='address'
        >
          <Input placeholder='Nhập địa chỉ' />
        </Form.Item>
        <Form.Item
          label='Có bán hàng'
          name='isShop'
        >
          <Checkbox
            checked={checkShop}
            onChange={onChangeShop}
          />
        </Form.Item>
      </Form>
    </Modal>
  )
}))

export default ModalForm