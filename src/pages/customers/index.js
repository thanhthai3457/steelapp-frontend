import React, { useState, useEffect, useMemo, useRef, useCallback } from 'react'
import Grid from 'components/grid/index.js'
import { graphql } from '@apollo/react-hoc'
import { GET_CUSTOMERS } from './gql'
import Modal from './modal'

const StoreShop = props => {
  const [dataCustomer, setDataCustomer] = useState([])
  const gridApi = useRef(null)
  const modalRef = useRef(null)

  useEffect(() => {
    if (props.data.loading) props.preloader.show()
    else {
      setTimeout(() => {
        props.preloader.hide()
      }, 2000)
    }
    if (props.data.getCustomers) {
      setDataCustomer(props.data.getCustomers)
    }
    return () => {
      props.preloader.hide()
    }
  }, [props.data, props.preloader])

  const handleAddClick = useCallback(() => {
    modalRef.current.open()
  }, [])

  const handleEditClick = useCallback((slt) => {
    modalRef.current.open(slt)
  }, [])

  const gridOptions = useMemo(() => ({
    paginationNumberFormatter: function(params) {
      return '[' + params.value.toLocaleString() + ']';
    },
    columnDefs: [
      {
        headerName: 'Mã KH',
        field: 'code'
      },
      {
        headerName: 'Tên KH',
        field: 'name'
      },
      {
        headerName: 'Mã số thuế',
        field: 'taxCode'
      },
      {
        headerName: 'Số điện thoại',
        field: 'mobile'
      },
      {
        headerName: 'Địa chỉ',
        field: 'address'
      }
    ],
    actionDefs: [
      {
        action: 'add',
        type: 'default',
        tooltip: 'Thêm KH mới',
        onClick: handleAddClick
      },
      {
        action: 'update',
        type: 'single',
        tooltip: 'Chỉnh sửa',
        onClick: (selectecRows) => handleEditClick(selectecRows[0])
      },
      {
        action: 'delete',
        type: 'multiple',
        onClick: (selectecRows) => console.log(selectecRows)
      }
    ],
    defaultColDef: {
      resizable: true,
      sortable: true,
      filter: true,
    },
    rowSelection: 'multiple',
    rowMultiSelectWithClick: true,
    floatingFilter: true,
    pagination: true,
    defaultPageSize: 50
  }), [handleAddClick, handleEditClick])

  return (
    <div>
      <div className='steelapp-toolbar'>
        <div className='steelapp-title-component'>Khách hàng</div>
      </div>
      <div className='steelapp-content'>
        <Grid
          {...gridOptions}
          data={dataCustomer}
          gridApi={gridApi}
        />
      </div>
      <Modal
        ref={modalRef}
      />
    </div>
  )
}

export default graphql(GET_CUSTOMERS)(StoreShop)