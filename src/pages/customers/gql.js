import gql from 'graphql-tag'

const GET_CUSTOMERS = gql`
  {
    getCustomers {
      _id
      code
      name
      mobile
      address
      taxCode
    }
  }
`

export { GET_CUSTOMERS }
